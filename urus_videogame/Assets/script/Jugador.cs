using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jugador : MonoBehaviour
{
    public float fuerzaSalto;
	public float velocidadMovimiento = 3.0f; // Ajusta la velocidad seg�n tus necesidades

	private Rigidbody2D rigidbody2D;
    private Animator animator;

	public GameManager gameManager;
    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        rigidbody2D = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
		if (Input.GetKeyDown(KeyCode.Space))
		{
			animator.SetBool("estaSaltando", true);
			animator.SetBool("enEspera", false);
			rigidbody2D.AddForce(new Vector2(0, fuerzaSalto));
		}
		else if (Input.GetKey(KeyCode.RightArrow))
		{
			animator.SetBool("adelante", true);
			animator.SetBool("atras", false);
			animator.SetBool("enEspera", false);
			rigidbody2D.velocity = new Vector2(velocidadMovimiento, rigidbody2D.velocity.y);
		}
		else if (Input.GetKey(KeyCode.LeftArrow))
		{
			animator.SetBool("adelante", false);
			animator.SetBool("atras", true);
			animator.SetBool("enEspera", false);
			rigidbody2D.velocity = new Vector2(-velocidadMovimiento, rigidbody2D.velocity.y);
		}
		else
		{
			animator.SetBool("adelante", false);
			animator.SetBool("atras", false);
			animator.SetBool("enEspera", true);
			rigidbody2D.velocity = new Vector2(0, rigidbody2D.velocity.y);
		}

	}

    private void OnCollisionEnter2D(Collision2D collision) {
        if (collision.gameObject.tag == "Suelo")
        {
            animator.SetBool("estaSaltando", true);
        }
		if (collision.gameObject.tag == "Obstaculo")
		{
			gameManager.Message = true;
		}
	}
}
