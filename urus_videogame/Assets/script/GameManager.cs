using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{

    public GameObject message;
    public GameObject col;
    public Renderer fondo;
    public GameObject nivel1;
    public List<GameObject> obstaculos;
    public bool Message = false;
	public bool start = false;

	public float velocidad = 2;
    // Start is called before the first frame update
    void Start()
    {
        //crear mapa
        for (int i = 0; i < 21; i++)
        {
            Instantiate(col, new Vector2(-10 + i, -4), Quaternion.identity);
        }

        obstaculos.Add(Instantiate(nivel1, new Vector2(10, -2), Quaternion.identity));
    }

    // Update is called once per frame
    void Update()
    {

        if (start == false) {
            if (Input.GetKeyDown(KeyCode.X))
            { 
                start= true;
            }
        }
		if (start == false && Message == true)
		{   
            message.SetActive(true);
			if (Input.GetKeyDown(KeyCode.X))
			{
                SceneManager.LoadScene(SceneManager.GetActiveScene().name);
			}
		}

		if (start == true && Message == false)
        {
            message.SetActive(false);
			fondo.material.mainTextureOffset = fondo.material.mainTextureOffset + new Vector2(0.02f, 0) * Time.deltaTime;

			for (int i = 0; i < obstaculos.Count; i++)
			{
				if (obstaculos[i].transform.position.x <= -10)
				{
					float randomObs = Random.Range(11, 15);
					obstaculos[i].transform.position = obstaculos[i].transform.position + new Vector3(-1, 0, 0) * Time.deltaTime * velocidad;
				}
			}
		}

	}

    
}

