﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





extern void GameManager_Start_m87A71D65F3171A58DBDDBFB03832ADA65643D0E2 (void);
extern void GameManager_Update_m7F29D8E933B8D21D2E67507979C0F12ACF87BB41 (void);
extern void GameManager__ctor_mF453CED520617BFB65C52405A964E06CF17DB368 (void);
extern void Jugador_Start_m4A8EEB904B232D87FFD83466B82FB8A8F7C34809 (void);
extern void Jugador_Update_m4EA928589E7D65B2D631659EC683C0705D656A48 (void);
extern void Jugador_OnCollisionEnter2D_m14B5260CE21D4B36334C8765466E76E230B2D8CD (void);
extern void Jugador__ctor_m5F2E37D796A67E1A0BB55B0038EDA7C865931DE4 (void);
extern void Readme__ctor_m69C325C4C171DCB0312B646A9034AA91EA8C39C6 (void);
extern void Section__ctor_m5F732533E4DFC0167D965E5F5DB332E46055399B (void);
extern void UnitySourceGeneratedAssemblyMonoScriptTypes_v1_Get_mBEB95BEB954BB63E9710BBC7AD5E78C4CB0A0033 (void);
extern void UnitySourceGeneratedAssemblyMonoScriptTypes_v1__ctor_mE70FB23ACC1EA12ABC948AA22C2E78B2D0AA39B1 (void);
static Il2CppMethodPointer s_methodPointers[11] = 
{
	GameManager_Start_m87A71D65F3171A58DBDDBFB03832ADA65643D0E2,
	GameManager_Update_m7F29D8E933B8D21D2E67507979C0F12ACF87BB41,
	GameManager__ctor_mF453CED520617BFB65C52405A964E06CF17DB368,
	Jugador_Start_m4A8EEB904B232D87FFD83466B82FB8A8F7C34809,
	Jugador_Update_m4EA928589E7D65B2D631659EC683C0705D656A48,
	Jugador_OnCollisionEnter2D_m14B5260CE21D4B36334C8765466E76E230B2D8CD,
	Jugador__ctor_m5F2E37D796A67E1A0BB55B0038EDA7C865931DE4,
	Readme__ctor_m69C325C4C171DCB0312B646A9034AA91EA8C39C6,
	Section__ctor_m5F732533E4DFC0167D965E5F5DB332E46055399B,
	UnitySourceGeneratedAssemblyMonoScriptTypes_v1_Get_mBEB95BEB954BB63E9710BBC7AD5E78C4CB0A0033,
	UnitySourceGeneratedAssemblyMonoScriptTypes_v1__ctor_mE70FB23ACC1EA12ABC948AA22C2E78B2D0AA39B1,
};
static const int32_t s_InvokerIndices[11] = 
{
	5101,
	5101,
	5101,
	5101,
	5101,
	4061,
	5101,
	5101,
	5101,
	7487,
	5101,
};
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	11,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
};
